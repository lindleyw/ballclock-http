package clock

import (
	"ballclock-http/structs/ball"
	"ballclock-http/structs/queue"
	"ballclock-http/structs/rail"
	"encoding/json"
	"log"
	"math"
)

// static ballholder capacities
const HOUR_RAIL_CAP = 11
const FIVE_MIN_RAIL_CAP = 11
const ONE_MIN_RAIL_CAP = 4

var q queue.Queue
var hourRail rail.Rail
var fiveMinRail rail.Rail
var oneMinRail rail.Rail
var nClockRefreshes uint64

func Init() {
	hourRail = rail.NewRail(HOUR_RAIL_CAP)
	fiveMinRail = rail.NewRail(FIVE_MIN_RAIL_CAP)
	oneMinRail = rail.NewRail(ONE_MIN_RAIL_CAP)
}

// Update the clock state by adding ball
func updateClockState(b ball.Ball) {
	var spilledBalls []ball.Ball

	spilledBalls = oneMinRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	q.Push(spilledBalls)

	spilledBalls = fiveMinRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	q.Push(spilledBalls)

	spilledBalls = hourRail.Push(b)
	if len(spilledBalls) == 0 {
		return
	}
	q.Push(append(spilledBalls, b))
}

// Detect a cycle occurrence in a ball clock and track time for that cycle to
// occur
func findCycle(queueCapacity uint8) {
	// break when the balls are all back in their original positions in the
	// queue
	for {
		ball := q.Pop()
		updateClockState(ball)
		if q.IsFull() {
			nClockRefreshes++
			if q.DoCycleCheck() {
				break
			}
		}
	}
}

func findRevolutions(revolutions uint64) {
	for {
		ball := q.Pop()
		updateClockState(ball)
		if q.DoBallRevCount(revolutions) {
			break
		}
	}
}

func GetDaysUntilCycle(queueCapacity uint8) uint64 {
	// Number of times the clock refreshes, i.e., the number of 12-hour
	// periods
	nClockRefreshes = 0
	q = queue.NewQueue(queueCapacity)
	findCycle(queueCapacity)
	// There 2 clock refreshes in a day
	return uint64(math.Ceil(float64(nClockRefreshes) / 2.0))
}

func CountRevsOfClock(queueCapacity uint64, revolutions uint64) string {
	type Time struct {
		Min     []int
		FiveMin []int
		Hour    []int
		Main    []int
	}
	q = queue.NewQueue(uint8(queueCapacity))
	findRevolutions(revolutions)

	status := Time{
		oneMinRail.Contains(),
		fiveMinRail.Contains(),
		hourRail.Contains(),
		q.Contains()}

	result, err := json.Marshal(status)
	if err != nil {
		log.Fatal(err)
	}
	return string(result)
}
