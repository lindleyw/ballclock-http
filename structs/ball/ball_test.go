package ball

import (
	"testing"
)

func TestNewBall(t *testing.T) {
	const ID = 0
	ball := New(ID)
	if ball.Id != ID {
		t.Errorf("Unexpected ball ID (Actual: %d, Expected: %d)\n", ball.Id, ID)
	}
}
