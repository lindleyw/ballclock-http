package ball

type Ball struct {
	Id uint8
}

func New(id uint8) Ball {
	return Ball{id}
}
