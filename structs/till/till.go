package till

type Till struct {
	// how much the Till can hold
	Capacity uint8
	// how much the Till is holding
	NCoins uint8
}

// Create a new Till
func NewTill(capacity uint8, nCoins uint8) Till {
	return Till{capacity, nCoins}
}

func (tl *Till) IsFull() bool {
	return tl.Capacity == tl.NCoins
}
