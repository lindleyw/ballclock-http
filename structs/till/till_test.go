package till

import (
	"testing"
)

func TestNewTill(t *testing.T) {
	const EXPECTED_CAPACITY = 4
	const EXPECTED_NCOINS = EXPECTED_CAPACITY
	tl := NewTill(EXPECTED_CAPACITY, EXPECTED_CAPACITY)
	if tl.Capacity != EXPECTED_CAPACITY {
		t.Errorf("Unexpected capacity (actual %d, expected %d)",
			tl.Capacity, EXPECTED_CAPACITY)
	}
	if tl.NCoins != EXPECTED_NCOINS {
		t.Errorf("Unexpected nCoins (actual %d, expected %d)",
			tl.NCoins, EXPECTED_NCOINS)
	}
	if !tl.IsFull() {
		t.Errorf("Expected Till to be full")
	}
	tl.NCoins--
	if tl.IsFull() {
		t.Errorf("Expected Till not to be full")
	}
}
