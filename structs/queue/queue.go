package queue

import (
	"ballclock-http/structs/ball"
	"ballclock-http/structs/till"
	"container/ring"
)

type Queue struct {
	till.Till
	Balls []ball.Ball
	ring  *ring.Ring
	count uint8
}

// Create a new, full, Till
func NewQueue(capacity uint8) Queue {
	bh := till.NewTill(capacity, capacity)
	balls := make([]ball.Ball, capacity)
	r := ring.New(int(capacity))
	c := 0
	for i := 0; i < capacity; i++ {
		r.Value, balls[i] = ball.New(i), ball.New(i)
		r = r.Next()
	}
	return Queue{bh, balls, r, c}
}

func (q *Queue) Pop() ball.Ball {
	q.NCoins--
	q.count++
	ball := q.ring.Value.(ball.Ball)
	q.ring = q.ring.Next()

	//remove ball from list
	if len(q.Balls) > 0 {
		q.Balls = append(q.Balls[:0], q.Balls[0+1:]...)
	}

	return ball
}

// Return true if the balls are in their original position in the queue
func (q *Queue) DoCycleCheck() bool {
	if !q.IsFull() {
		return false
	}
	tmp := q.ring
	for i := 0; i < q.Capacity; i++ {
		ball := q.ring.Value.(ball.Ball)
		q.ring = q.ring.Next()
		if ball.Id != i {
			// restore ring
			q.ring = tmp
			return false
		}
	}
	return true
}

// Return true if given so many balls have passed
func (q *Queue) DoBallRevCount(count uint64) bool {
	if q.count == count {
		return true
	}
	return false
}

// Put an array of balls back to the end of the queue
func (q *Queue) Push(balls []ball.Ball) {
	tmp := q.ring
	q.ring = q.ring.Move(int(q.NCoins))
	for i := range balls {
		q.NCoins++
		q.ring.Value = balls[i]
		q.ring = q.ring.Next()

	}
	//append balls back onto the slice
	q.Balls = append(q.Balls, balls...)
	q.ring = tmp
}

func (q *Queue) Contains() []int {
	balls := make([]int, 0, len(q.Balls))
	for i := range q.Balls {
		if i < int(q.NCoins) {
			balls = append(balls, int(q.Balls[i].Id+1))
		}
	}
	return balls
}
