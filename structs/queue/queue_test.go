package queue

import (
	"ballclock-http/structs/ball"
	"testing"
)

func TestNewQueue(t *testing.T) {
	const EXPECTED_CAPACITY = 11
	const EXPECTED_NCOINS = EXPECTED_CAPACITY
	q := NewQueue(EXPECTED_CAPACITY)
	if q.Capacity != EXPECTED_CAPACITY {
		t.Errorf("Unexpected capacity (actual %d, expected %d)",
			q.Capacity, EXPECTED_CAPACITY)
	}
	if q.NCoins != EXPECTED_NCOINS {
		t.Errorf("Unexpected nBalls (actual %d, expected %d)",
			q.NCoins, EXPECTED_NCOINS)
	}
	if q.ring.Len() != EXPECTED_CAPACITY {
		t.Errorf("Unexpected length of ring (actual %d, expected %d)",
			q.ring.Len(), EXPECTED_CAPACITY)
	}
	// Go through ring twice to test it
	for i := 0; i < 2; i++ {
		for i := 0; i < q.ring.Len(); i++ {
			if q.ring.Value.(ball.Ball).Id != uint8(i) {
				t.Errorf("Ball out of order (actual %d, expected %d)",
					q.ring.Value.(ball.Ball).Id,
					i)
			}
			q.ring = q.ring.Next()
		}
	}
}
