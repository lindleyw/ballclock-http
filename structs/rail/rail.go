package rail

import (
	"ballclock-http/structs/ball"
	"ballclock-http/structs/till"
)

type Rail struct {
	till.Till
	Balls []ball.Ball
}

// Create a new, empty, Rail
func NewRail(capacity uint8) Rail {
	tl := till.NewTill(capacity, 0)
	balls := make([]ball.Ball, capacity)
	return Rail{tl, balls}
}

// Empty the ball holder and return a reversed list of the spilt Balls
func (r *Rail) run() []ball.Ball {
	spilledBalls := make([]ball.Ball, r.Capacity)
	for i := range r.Balls {
		spilledBalls[r.Capacity-1-uint8(i)] = r.Balls[i]
	}
	//Clean out the balls
	r.Balls = make([]ball.Ball, r.Capacity)
	return spilledBalls
}

// Add a ball to the rail.  If the rail is full, it will spill.
// A slice of spilled balls is returned.
func (r *Rail) Push(b ball.Ball) []ball.Ball {
	if r.IsFull() {
		// Reset state and spill
		r.NCoins = 0
		return r.run()
	}
	r.Balls[r.NCoins] = b
	r.NCoins++
	return []ball.Ball{}
}

func (r *Rail) Contains() []int {
	balls := make([]int, 0, len(r.Balls))
	for i := range r.Balls {
		if i < int(r.NCoins) {
			balls = append(balls, int(r.Balls[i].Id+1))
		}
	}
	return balls
}
