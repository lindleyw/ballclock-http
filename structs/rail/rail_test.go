package rail

import (
	"ballclock-http/structs/ball"
	"fmt"
	"testing"
)

func TestNewRail(t *testing.T) {
	const EXPECTED_CAPACITY = 11
	const EXPECTED_NCOINS = 0
	r := NewRail(EXPECTED_CAPACITY)
	if r.Capacity != EXPECTED_CAPACITY {
		t.Errorf("Unexpected Capacity (actual %d, expected %d)",
			r.Capacity, EXPECTED_CAPACITY)
	}
	if r.NCoins != EXPECTED_NCOINS {
		t.Errorf("Unexpected NCoins (actual %d, expected %d)",
			r.NCoins, EXPECTED_NCOINS)
	}
}

func TestRailPushAndSpill(t *testing.T) {
	const EXPECTED_CAPACITY = 4
	const EXPECTED_NCOINS = 0
	var spilledBalls []ball.Ball
	r := NewRail(EXPECTED_CAPACITY)

	// Push a ball with an ID of 1, and check the Balls slice
	spilledBalls = r.Push(ball.New(1))
	if r.Balls[0].Id != 1 {
		t.Errorf("Unexpected ball ID after rail push (actual %d, expected %d)",
			r.Balls[0].Id, 1)
	}
	// Nothing should have spilled
	if len(spilledBalls) != 0 {
		t.Errorf("Unexpected spilled balls (%v), expected no spillage", spilledBalls)
	}

	// Push a three more balls
	spilledBalls = r.Push(ball.New(2))
	if len(spilledBalls) != 0 {
		t.Errorf("Unexpected spilled balls (%v), expected no spillage", spilledBalls)
	}
	spilledBalls = r.Push(ball.New(3))
	if len(spilledBalls) != 0 {
		t.Errorf("Unexpected spilled balls (%v), expected no spillage", spilledBalls)
	}
	spilledBalls = r.Push(ball.New(4))
	if len(spilledBalls) != 0 {
		t.Errorf("Unexpected spilled balls (%v), expected no spillage", spilledBalls)
	}
	for i := range r.Balls {
		// i + 1, because we started at 1 to distinguish between test
		// the zero-value of the array
		if r.Balls[i].Id != uint8(i+1) {
			t.Errorf("Unexpected ball ID after rail push (actual %d, expected %d)",
				r.Balls[i].Id, i+1)
		}
	}

	if !r.IsFull() {
		t.Fatalf("Expected rail to be full")
	}

	// OK, rail is full and thus spill on the next push
	spilledBalls = r.Push(ball.New(5))
	if len(spilledBalls) == 0 {
		t.Errorf("Expected spilled balls, but got no spillage")
	}
	expected := []ball.Ball{ball.New(4), ball.New(3), ball.New(2), ball.New(1)}
	if fmt.Sprintf("%v", spilledBalls) != fmt.Sprintf("%v", expected) {
		t.Errorf("Unexpected spilled balls:\n"+
			"Actual: %v\n"+
			"Expected: %v",
			spilledBalls,
			expected)
	}
}
