#Ballclock HTTP

An implementation to the ball clock problem. You can read more about it here: [Ballclock Problem](http://www.chilton.com/~jimw/ballclk.html#[1])

This implementation goes beyond the original solution and solves for ball rotations. Given a number of balls, and a count of balls added to the clock, what is the exact order of the balls and their positions within the clock.

##Building

In order to get the binary built and running, you will need a proper GOPATH, and running the following commands from within the root of the folder.

```sh
go get && go run main.go
```

This will load up a local web server that you can hit from

```
localhost:8080
```

##Input

To get receive the amount of time it would take for the clock to reorganize it's order back to the original position, you can send a request with just a number of balls:

```
localhost:8080/:balls
```

To receive the exact positions of the balls with a ball count and a revolutions count, send the balls and the revolutions.

```
localhost:8008/:balls/:revolutions
```