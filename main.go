package main

import (
	"ballclock-http/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func Clock(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var revs uint64
	var balls uint64
	var err error

	if balls, err = strconv.ParseUint(vars["balls"], 10, 8); err != nil {
		// pass
	}

	if !ballValidation(balls, w) {
		return
	}

	if vars["revs"] != "" {
		if revs, err = strconv.ParseUint(vars["revs"], 10, 32); err != nil {
			log.Fatal(err)
		}
	}

	if revs == 0 {
		clock.Init()
		var result = clock.GetDaysUntilCycle(uint8(balls))
		w.Write([]byte(strconv.FormatUint(balls, 10) + " balls cycle after " + strconv.FormatUint(result, 10) + " days"))
	} else {
		clock.Init()
		var result = clock.CountRevsOfClock(balls, revs)
		w.Write([]byte(result))
	}

}

func ballValidation(balls uint64, w http.ResponseWriter) bool {
	if balls < 27 {
		w.Write([]byte("A ball clock can not operate on less than 27 balls"))
		return false
	} else if balls > 127 {
		w.Write([]byte("A ball clock can not operate on more than 127 balls"))
		return false
	}
	return true
}

func main() {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", Clock)
	router.HandleFunc("/{balls}", Clock)
	router.HandleFunc("/{balls}/{revs}", Clock)

	log.Fatal(http.ListenAndServe(":8080", router))
}
